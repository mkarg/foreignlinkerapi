package eu.headcrashing;

import com.sun.jna.Native;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

public class MyClassJNA {

	interface Kernel32 extends StdCallLibrary {

		static final Kernel32 INSTANCE = Native.load("Kernel32", Kernel32.class, W32APIOptions.DEFAULT_OPTIONS);
	
		int lstrlen(String text);
	}

    public static int myMethod(String text) {
        return Kernel32.INSTANCE.lstrlen(text);
	}
}
